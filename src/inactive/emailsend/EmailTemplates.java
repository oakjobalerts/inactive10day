package inactive.emailsend;

import inactive.models.UsersData;

import org.apache.commons.codec.binary.Base64;

public class EmailTemplates {

	public String inActive10DayTemplate(UsersData userDataObject) {

		userDataObject.keyword = userDataObject.keyword.toLowerCase();

		String keyword = "";
		String locationString = "";
		String inactive_template_body = "";

		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null") && !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			locationString = userDataObject.city + ", " + userDataObject.state;
		} else {
			locationString = userDataObject.zipcode;
		}

		String unsubString = "http://" + userDataObject.domainName + "/unsubscribe.php?alert_id=" + baseEncode64(userDataObject.id)// $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml) + "&email=" + baseEncode64(userDataObject.email);

		String color = userDataObject.themeColor;

		// by pawan for those who are having keyword
		if (!userDataObject.keyword.equalsIgnoreCase("")) {
			keyword = capalizedFirstCharOne(userDataObject.keyword);
			inactive_template_body = "<tr><td>" + "<p style=\"margin:0 0 15px 0;\">" + userDataObject.HostDomainName + " is committed to helping you find the job you love. If you are no longer interested in receiving job alerts for <strong>" + keyword + "</strong> jobs " + "in <strong>" + locationString + "</strong>, you can " + "edit  your  job  alert here</p>" + "</td></tr>";
		} else {

			inactive_template_body = "<tr><td>" + "<p style=\"margin:0 0 15px 0;\">" + userDataObject.HostDomainName + " is committed to helping you find the job you love. " + "If you are no longer interested in receiving job alerts for jobs in " + "<strong> " + locationString + "</strong> , you can edit your job alert here. " + "</strong></p>" + "</td></tr>";

		}

		// SettingClass.count++;
		/*
		 * for Top header with logo
		 */

		String html = "<div style=\"background-color:#f3f3f3;font-size:14px;font-family:Arial,sans-serif;\">" + "<div style=\"background:#dddddd;border-bottom:solid 2px " + color + ";border-top:solid 2px " + color + "\">" + "<table style=\"background-color:#dddddd\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\">" + "<tbody><tr>" + "<td height=\"45\" valign=\"middle\">" + "<a title='" + userDataObject.domainName + "' href=\"http://" + userDataObject.domainName
				+ "\" target=\"_blank\">" + "<img style=\"padding: 5px;\" src=\"" + userDataObject.logoUrl + "\" height=\"50\"  alt=\"" + userDataObject.HostDomainName + "\"> </a>" + "</td>" + "<td height=\"45\" valign=\"middle\"  align=\"right\">" + "<p style=\"color:#2e2e2e;font-family:Arial,sans-serif;font-size:16px;margin:0px;\">Are You Still Searching for Jobs?</p>" + "</td></tr></tbody></table></div>";

		/*
		 * for message body
		 */

		html += " <table style=\"background-color:#fff; border:solid 1px #ddd; margin-top:15px; padding:15px 14px;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\">" + "<tbody><tr><td height=\"45\" valign=\"middle\">" + "<h1 style=\"font-weight:bold;font-size:18px;color:#2c2c2c;margin:1px 0 5px;border-bottom: 1px solid #e7e7e7; padding-bottom:7px;\"> We noticed you haven't been using your job alert lately... </h1>" + "</td></tr>" + inactive_template_body
				+ "<tr><td> <div style=\"width:100%; float:left;\">" + "<a target=\"_blank\" title='" + userDataObject.domainName + "' style=\"padding:10px 43px;color:#ffffff;white-space:nowrap; float:left;font-size:12px;font-weight:bold;text-decoration:none;display:block;background:" + color + "; border-radius:2px;\" href=\"http://" + userDataObject.domainName + "/update_alert.php?id=" + baseEncode64(userDataObject.id) + "&amp;source=" + baseEncode64(userDataObject.userSourceNameForHtml)
				+ "\"> Edit Job Alert </a>" + "</div><div style=\"float:left; width:100%;\">" + "<p style=\"font-family:Arial,sans-serif;font-size:14px;color:#2c2c2c;line-height:18px; margin:10px 0 0;\"> " + "<b>Get Hired. Love Your Job.</b> " + "<br> 	The " + userDataObject.HostDomainName + " Team </p>" + "</div></td></tr>" + "</tbody></table>";

		/*
		 * for footer body
		 */

		html += "<div style=\"margin:15px 0;\">" + "<p style=\"font-family:Arial,sans-serif;font-size:10px;color:#888888;line-height:18px;margin:0;margin-top:0px;margin-bottom:0px;text-align:center\"> " + userDataObject.HostDomainName + " &nbsp;|&nbsp; " + userDataObject.postalAddress + "<br>This message was sent to <a title='" + userDataObject.domainName + "' target=\"_blank\" style=\"color:#0066cc;text-decoration:none\" href=\"mailto:" + userDataObject.email + "\">" + userDataObject.email
				+ "</a>. " + "<br>" + "<a target=\"_blank\" title='" + userDataObject.domainName + "' style=\"color:#0066cc;text-decoration:none\" " + "href=\"http://" + userDataObject.domainName + "/jobs.php?q=" + userDataObject.keyword + "&l=" + userDataObject.locationString + "\">Browse Jobs</a> " + "&nbsp;|&nbsp; " + "<a target=\"_blank\" title='" + userDataObject.domainName + "' style=\"color:#0066cc;text-decoration:none\" " + "href=\"" + unsubString + "\">Unsubscribe</a><br></p></div>";

		html += "</div>" + userDataObject.unsubTemplink;
		return html;

	}

	public String inActive10DayPlainTextTemplate(UsersData userDataObject) {
		String keyword = "";
		String inactive_template_body = "";
		String locationString = "";
		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null") && !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			locationString = userDataObject.city + ", " + userDataObject.state;
		} else {
			locationString = userDataObject.zipcode;
		}

		String editThisAlerts = "http://" + userDataObject.domainName + "/update_alert.php?id=" + baseEncode64(userDataObject.id) + "&amp;source=" + baseEncode64(userDataObject.userSourceNameForHtml);
		String privacyLink = "http://" + userDataObject.domainName + "/jobs.php?q=" + userDataObject.keyword + "&l=" + userDataObject.locationString;
		String unsubString = "http://" + userDataObject.domainName + "/unsubscribe.php?alert_id=" + baseEncode64(userDataObject.id)// $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml) + "&email=" + baseEncode64(userDataObject.email);

		String textHtml = "Are You Still Searching for Jobs?\n\n" + "We noticed you haven't been using your job alert lately...\n\n";

		// by pawan for those who are having keyword
		if (!userDataObject.keyword.equalsIgnoreCase("")) {
			keyword = capalizedFirstCharOne(userDataObject.keyword);
			inactive_template_body = userDataObject.HostDomainName + " is committed to helping you find the job you love. If you are no longer interested in receiving job alerts for " + keyword + "jobs " + "in " + locationString + ", you can " + "edit  your  job  alert here";
		} else {

			inactive_template_body = userDataObject.HostDomainName + " is committed to helping you find the job you love. If you are no longer interested in receiving job alerts for jobs in " + locationString + " you can edit your job alert here. ";

		}

		textHtml += inactive_template_body + "\n\n" + editThisAlerts + "\n\n";

		textHtml += "Get Hired. Love Your Job.\n" + "The " + userDataObject.HostDomainName + " Team \n\n";

		textHtml += userDataObject.HostDomainName + " | " + userDataObject.postalAddress + "\n" + "This message was sent to " + userDataObject.email + ".\n\n";

		textHtml += "Browse Jobs\n" + privacyLink + "\n\n";

		textHtml += "Unsubscribe\n" + unsubString + "\n\n";

		return textHtml;

	}

	public String capalizedFirstCharOne(String str) {
		char[] charArray = str.toCharArray();
		charArray[0] = Character.toUpperCase(charArray[0]);
		for (int i = 1; i < charArray.length; i++) {
			if (charArray[i] == ' ') {
				charArray[i + 1] = Character.toUpperCase(charArray[i + 1]);
			}
		}

		String result = new String(charArray);
		if (result == null)
			return str;
		return result;
	}

	private String baseEncode64(String strToEncode) {
		byte[] encoded = Base64.encodeBase64(strToEncode.getBytes());
		return new String(encoded);

	}

}
