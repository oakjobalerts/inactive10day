package inactive.emailsend;

import inactive.main.SettingsClass;
import inactive.main.Utility;
import inactive.models.GroupObject;
import inactive.models.UsersData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.sendgrid.SendGrid;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.AddressAttributes;
import com.sparkpost.model.RecipientAttributes;
import com.sparkpost.model.TemplateContentAttributes;
import com.sparkpost.model.TransmissionWithRecipientArray;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.RestConnection;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.impl.MultiPartWriter;

public class SendEmail {
	AmazonSimpleEmailServiceClient emailSendClient;
	String awsAccessKeyForSES = "AKIAJB4W4NEOUTCHYQQA";
	String awsSecretKeyForSES = "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";
	BasicAWSCredentials awsCredentialsForSES;
	public List<String> bccMailList = new ArrayList<String>();

	public SendEmail() {

		Utility.createDomainWiseHashMap();
		bccMailList.clear();
		bccMailList.add("mangesh@mailtesting.us");
		bccMailList.add("gaurav@mailtesting.us");
		bccMailList.add("pawan@mailtesting.us");
		// bccMailList.add("jason@mailtesting.us");
		// bccMailList.add("amit@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("raj@mailtesting.us");

		awsCredentialsForSES = new BasicAWSCredentials(awsAccessKeyForSES, awsSecretKeyForSES);
		emailSendClient = new AmazonSimpleEmailServiceClient(awsCredentialsForSES);
		emailSendClient.setRegion(Region.getRegion(Regions.US_WEST_2));
	}

	EmailTemplates mEmailTemplates = new EmailTemplates();

	public void writeEmailStats(UsersData userDataObject) {

		try {
			SettingsClass.memcacheObj.set(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_" + userDataObject.zipcode.replace(" ", "") + "_" + SettingsClass.queueName + "_" + userDataObject.whitelabel_name, 0, "1");

		} catch (Exception e1) {

			e1.printStackTrace();
		}

		try {
			SettingsClass.totalEmailCount.incrementAndGet();
			SettingsClass.newTotalEmailCount.incrementAndGet();
			SettingsClass.BccMailsCount.incrementAndGet();

			try {
				SettingsClass.memcacheObj.set(SettingsClass.emailSend, 0, String.valueOf(SettingsClass.totalEmailCount)); //
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		synchronized (SettingsClass.child_Process_stats_hashMap) {

			try {

				Integer childEmailSend = null;

				String tempKey = "";
				if (userDataObject.sendGridCategory.contains("default value")) {
					tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
				} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
					tempKey = "_" + userDataObject.comapaingId.replace(" ", "").trim();
				} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
					tempKey = "_" + userDataObject.comapaingId.replace(" ", "").trim();
				} else {
					tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
				}

				String childKeyStr = userDataObject.domainName.replaceAll(" ", "").trim() + tempKey + "_" + SettingsClass.emailSend;
				// for child domain emailSend processed count...

				try {

					childEmailSend = SettingsClass.child_Process_stats_hashMap.get(childKeyStr);
					if (childEmailSend == null) {
						childEmailSend = 0;

					}
				} catch (Exception e) {
					// TODO: handle exception
					childEmailSend = 0;
				}

				childEmailSend++;

				SettingsClass.memcacheObj.set(childKeyStr, 0, String.valueOf(childEmailSend));

				// setting it in the hash map

				SettingsClass.child_Process_stats_hashMap.put(childKeyStr, childEmailSend);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (SettingsClass.isTestRun && SettingsClass.totalEmailCount.get() >= SettingsClass.numberOfTestingMails) {
			System.exit(0);
		}

	}

	public void sendEmailViaMailgun(UsersData userDataObject, int localGroupCount) {

		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(MultiPartWriter.class);
		Client client = Client.create(cc);
		client.addFilter(new HTTPBasicAuthFilter("api", "key-b8f6462082ffa67a3a56352b14f91523"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + userDataObject.domainName + "/messages");
		// MultivaluedMapImpl formData = new MultivaluedMapImpl();
		webResource.setProperty("domain", "" + userDataObject.domainName + "");
		FormDataMultiPart form = new FormDataMultiPart();
		// List<String> dataList = composeClearfitEmail(userDataObject,
		// jobsArray);

		String html = "";

		// setting location string
		settingLocationString(userDataObject);

		html = mEmailTemplates.inActive10DayTemplate(userDataObject);

		if (SettingsClass.isTestRun)
			form.field("to", SettingsClass.testingEmailId);
		else
			form.field("to", userDataObject.email);

		if (!SettingsClass.isTestRun && (SettingsClass.BccMailsCount.get() == 0 || SettingsClass.BccMailsCount.get() >= SettingsClass.bccSendEmailIntervalCount)) {

			for (int i = 0; i < bccMailList.size(); i++) {
				form.field("bcc", bccMailList.get(i));
			}

			SettingsClass.BccMailsCount.set(1);

		}
		JSONObject jObj = new JSONObject();

		try {
			jObj.put("group_id", "2000");
			jObj.put("template_id", "2000");
			jObj.put("SENTDATE", SettingsClass.dateFormat.format(new Date()));
			jObj.put("DOMAIN", userDataObject.domainName);
			jObj.put("inactive10dayuser", "y");

		} catch (JSONException e3) {

			e3.printStackTrace();
		}

		GroupObject groupObject = groupSubjectLineObject(userDataObject, 1, localGroupCount);

		form.field("from", userDataObject.HostDomainName + "<" + userDataObject.fromDomainName + ">");
		form.field("subject", groupObject.getSubject());

		form.field("v:my-custom-data", jObj.toString());// json value

		// form.field("o:campaign", userDataObject.comapaingId);

		form.field("o:tag", userDataObject.comapaingId);

		form.field("html", html);

		String textVersion = mEmailTemplates.inActive10DayPlainTextTemplate(userDataObject);

		// new tags
		form.field("text", textVersion);
		form.field("stripped-html", html);
		form.field("body-html", html);
		form.field("body-plain", textVersion);
		form.field("content-type", MediaType.TEXT_HTML);

		try {
			ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, form);

			if (response.getStatus() == 200) {
				System.out.println("email=" + userDataObject.email);
				System.out.println("Email sent Via Mailgun with domain=" + userDataObject.domainName);
				writeEmailStats(userDataObject);

			} else {
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void sendEmailViaSendGrid(UsersData userDataObject) {

		String html = "";

		// setting location string
		settingLocationString(userDataObject);

		html = mEmailTemplates.inActive10DayTemplate(userDataObject);

		// GET FROM HASHMAP

		// parameterModelClassObject.getSendgridUsername(),
		// parameterModelClassObject.getSendgridPassword()

		SendGrid sendgrid = new SendGrid(userDataObject.sendGridUserName, userDataObject.sendGridUserPassword);
		SendGrid.Email email = new SendGrid.Email();

		email.setFrom(userDataObject.fromDomainName);
		try {
			email.addUniqueArg("GROUPID", "2000");
			email.addUniqueArg("TEMPLATEID", "2000");
			email.addUniqueArg("SENTDATE", String.valueOf(SettingsClass.dateFormat.format(new Date())));
			email.addCategory(userDataObject.sendGridCategory);
			email.addUniqueArg("DOMAIN", userDataObject.domainName);
			email.addUniqueArg("inactive10dayuser", "y");
		} catch (Exception e) {
		}

		String subject = "It's Been A While...";
		if (!userDataObject.firstName.equalsIgnoreCase(""))
			subject = userDataObject.firstName + ", " + subject;

		email.setSubject(subject);
		email.setFromName(userDataObject.HostDomainName);

		email.setHtml(html);

		String textHtml = mEmailTemplates.inActive10DayPlainTextTemplate(userDataObject);
		email.setText(textHtml);

		if (SettingsClass.isTestRun)
			email.addTo(SettingsClass.testingEmailId);
		else
			email.addTo(userDataObject.email);

		String[] bccArray = new String[bccMailList.size()];

		if (!SettingsClass.isTestRun && (SettingsClass.BccMailsCount.get() == 0 || SettingsClass.BccMailsCount.get() >= SettingsClass.bccSendEmailIntervalCount)) {
			System.out.println("bcc sending.....");
			for (int i = 0; i < bccMailList.size(); i++) {
				bccArray[i] = bccMailList.get(i);
			}
			email.setBcc(bccArray);
			SettingsClass.BccMailsCount.set(1);
		}

		try {
			// sending mail...
			SendGrid.Response response = sendgrid.send(email);

			sendgrid = null;
			email = null;

			if (response.getStatus()) {
				System.out.println("Email sent Via Sendgrid " + userDataObject.domainName);
				writeEmailStats(userDataObject);

			} else {
				System.out.println("The email was not sent." + response.getMessage());

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEmailVieSes(UsersData userDataObject) {
		SendEmailRequest request;
		String html = "";

		// setting location string
		settingLocationString(userDataObject);

		html = mEmailTemplates.inActive10DayTemplate(userDataObject);

		Destination destination = new Destination();

		if (SettingsClass.isTestRun)
			destination.withToAddresses(new String[] { SettingsClass.testingEmailId });
		else
			destination.withToAddresses(new String[] { userDataObject.email });

		if (!SettingsClass.isTestRun && (SettingsClass.BccMailsCount.get() == 0 || SettingsClass.BccMailsCount.get() >= SettingsClass.bccSendEmailIntervalCount)) {
			destination.withBccAddresses(bccMailList);
			System.out.println("bcc sending....");
			SettingsClass.BccMailsCount.set(1);
		}

		String subject1 = "It's Been A While...";
		if (!userDataObject.firstName.equalsIgnoreCase(""))
			subject1 = userDataObject.firstName + ", " + subject1;

		Content subject = new Content().withData(subject1);
		Content htmlbody = new Content().withData(html);
		Content htmlText = new Content().withData(mEmailTemplates.inActive10DayPlainTextTemplate(userDataObject));
		Body body = new Body().withHtml(htmlbody);

		body.withText(htmlText);

		// Create a message with the specified subject and body.
		com.amazonaws.services.simpleemail.model.Message message = new com.amazonaws.services.simpleemail.model.Message(subject, body);

		request = new SendEmailRequest(userDataObject.HostDomainName + "<" + userDataObject.fromDomainName + "> ", destination, message);

		try {
			emailSendClient.sendEmail(request);
			System.out.println("Email sent via ses");
			writeEmailStats(userDataObject);

			subject = null;
			htmlbody = null;
			body = null;
			request = null;

		} catch (Exception ex) {
			System.out.println("The email was not sent. for id=" + userDataObject.email);
			System.out.println("Error message: " + ex.getMessage());
			try {
				if (ex.getMessage().contains("Daily message quota exceeded")) {
					Thread.sleep(1800000);
				} else if (ex.getMessage().contains("Maximum sending rate exceeded")) {
					Thread.sleep(1800000);
				}
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}
	}

	public void sendEmailViaSparkPost(UsersData userDataObject) {

		// List<String> dataList = composeEmailFormat(userDataObject, jobsList);

		String html = "";

		//
		com.sparkpost.Client sparkpostClient = null;

		//
		try {
			sparkpostClient = newConfiguredClient(userDataObject);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();
		// Populate Recipients
		List<RecipientAttributes> recipientArray = new ArrayList<RecipientAttributes>();
		// never modify this variable this act as a to-email for the sparkpost
		String emailStr = "";
		// sending mail to email id..

		if (SettingsClass.isTestRun) {

			RecipientAttributes recipientAttribs = new RecipientAttributes();
			recipientAttribs.setAddress(new AddressAttributes(SettingsClass.testingEmailId));
			recipientArray.add(recipientAttribs);
			emailStr = SettingsClass.testingEmailId;
		} else {
			RecipientAttributes recipientAttribs = new RecipientAttributes();
			recipientAttribs.setAddress(new AddressAttributes(userDataObject.email));
			recipientArray.add(recipientAttribs);
			emailStr = userDataObject.email;
		}

		Map<String, String> metaData = null;
		try {
			metaData = new HashMap<String, String>();
			metaData.put("GROUPID", "2000");
			metaData.put("TEMPLATEID", "2000");
			metaData.put("SENTDATE", SettingsClass.dateFormat.format(new Date()));
			metaData.put("DOMAIN", userDataObject.domainName);
			metaData.put("inactive10dayuser", "y");
		} catch (Exception e) {
			// TODO: handle exception
		}
		settingLocationString(userDataObject);

		String textVersion = mEmailTemplates.inActive10DayPlainTextTemplate(userDataObject);
		html = mEmailTemplates.inActive10DayTemplate(userDataObject);

		String subject = "It's Been A While...";
		if (!userDataObject.firstName.equalsIgnoreCase(""))
			subject = userDataObject.firstName + ", " + subject;
		// Populate Email Body
		TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
		contentAttributes.setFrom(new AddressAttributes(userDataObject.fromDomainName, userDataObject.HostDomainName, emailStr));
		contentAttributes.setSubject(subject);

		contentAttributes.setText(textVersion);
		contentAttributes.setHtml(html);

		transmission.setContentAttributes(contentAttributes);
		transmission.setCampaignId(userDataObject.comapaingId);
		transmission.setMetadata(metaData);

		if (!SettingsClass.isTestRun && (SettingsClass.BccMailsCount.get() == 0 || SettingsClass.BccMailsCount.get() >= SettingsClass.bccSendEmailIntervalCount)) {

			// ADDING BCC TO USERS
			String[] bccArray = new String[bccMailList.size()];
			System.out.println("bcc sending.....");
			for (int i = 0; i < bccMailList.size(); i++) {
				bccArray[i] = bccMailList.get(i);
			}

			for (String bcc : bccArray) {
				RecipientAttributes bcc_recipientAttribs1 = new RecipientAttributes();
				AddressAttributes bcc_addressAttribs1 = new AddressAttributes(bcc);
				bcc_addressAttribs1.setHeaderTo(userDataObject.email);
				bcc_recipientAttribs1.setAddress(bcc_addressAttribs1);
				recipientArray.add(bcc_recipientAttribs1);
			}
			SettingsClass.BccMailsCount.set(1);
		}

		try {

			transmission.setRecipientArray(recipientArray);
			// sending mail...
			RestConnection connection = new RestConnection(sparkpostClient, RestConnection.defaultApiEndpoint);
			Response response = ResourceTransmissions.create(connection, 0, transmission);

			// Logger.getRootLogger().setLevel(Level.DEBUG);
			//
			// logger.debug("Transmission Response: " + response);

			// System.out.println("response" + response.getStatus()+"");

			if (response.getResponseCode() == 200) {
				System.out.println("Email sent via sparkpost");
				writeEmailStats(userDataObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected static com.sparkpost.Client newConfiguredClient(UsersData userDataObject) throws SparkPostException, IOException {
		// Client client = new Client(API_KEY);
		com.sparkpost.Client client = new com.sparkpost.Client(userDataObject.sparkpost_key);
		if (StringUtils.isEmpty(client.getAuthKey())) {
			throw new SparkPostException("SPARKPOST_API_KEY must be defined in  CONFIG_FILE .");
		}
		// client.setFromEmail(properties.getProperty("SPARKPOST_SENDER_EMAIL"));
		// if (StringUtils.isEmpty(client.getFromEmail())) {
		// throw new
		// SparkPostException("SPARKPOST_SENDER_EMAIL must be defined in " +
		// CONFIG_FILE + ".");
		// }

		System.out.println("Sending email through sparkpost");

		return client;
	}

	public static void settingLocationString(UsersData userDataObject) {

		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null") && !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			userDataObject.locationString = " " + userDataObject.city + ", " + userDataObject.state;
		} else {
			userDataObject.locationString = " " + userDataObject.zipcode;
		}

		userDataObject.locationString = userDataObject.locationString.trim();
	}

	public static GroupObject groupSubjectLineObject(UsersData userDataObject, int localTemplateCount, int localGroupCount) {

		if (localGroupCount >= SettingsClass.subjectLIneList.size())
			localGroupCount = 0;

		GroupObject groupObject = SettingsClass.subjectLIneList.get(localGroupCount);
		if (!userDataObject.firstName.equalsIgnoreCase(""))
			groupObject.setSubject(groupObject.getSubject().replace("Name", userDataObject.firstName));

		else {
			// here we are doing it in two ways because we have name with comma
			// and with name
			groupObject.setSubject(groupObject.getSubject().replace("Name,", userDataObject.firstName));
			groupObject.setSubject(groupObject.getSubject().replace("Name", userDataObject.firstName));

		}
		groupObject.setFromName(groupObject.getFromName().replace("whitelable_name", userDataObject.whitelabel_name));

		System.out.println(groupObject.getSubject());
		return groupObject;

	}
}
