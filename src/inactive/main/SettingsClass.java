package inactive.main;

import inactive.models.GroupObject;
import inactive.models.WhiteLabelParameterModel;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import net.spy.memcached.MemcachedClient;

import com.amazonaws.services.sqs.AmazonSQSClient;

public class SettingsClass {

	public static int TOTAL_N0_THREAD = 1;
	public static boolean isTestRun = true;
	// public static String queueName = "inactive-ten-day-users";
	public static String queueName = "inactive-ten-day-users";
	// public static String testingEmailId = "pawan@signitysolutions.co.in";
	public static String testingEmailId = "gurpreet.s@signitysolutions.in";

	public static int numberOfTestingMails = 5;

	// public static String filePath = "/var/nfs-93/redirect/mis_logs/";
	// public static String filePath = "/home/parveen/Desktop/aws_stats/";
	public static String filePath = "/home/signity/aws_stats/";

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static HashMap<String, WhiteLabelParameterModel> doimanWiseHashMap = new HashMap<String, WhiteLabelParameterModel>();
	static String baseQueurl = "https://sqs.us-east-1.amazonaws.com/306640124653/";
	public static Calendar cal;
	public static DecimalFormat decFormat = new DecimalFormat("###.######");
	public static SimpleDateFormat processTimeDateFormat = new SimpleDateFormat("hh:mm aaa");

	public static AtomicInteger userProcessingCount = new AtomicInteger(0);
	public static AtomicInteger newuserProcessingCount = new AtomicInteger(0);
	public static AtomicInteger totalEmailCount = new AtomicInteger(0);
	public static AtomicInteger newTotalEmailCount = new AtomicInteger(0);
	public static AtomicInteger emptyUserMessageCount = new AtomicInteger(0);
	public static AtomicInteger duplicateUser = new AtomicInteger(0);
	public static AtomicInteger emptyQueueCount = new AtomicInteger(0);
	public static AtomicInteger BccMailsCount = new AtomicInteger(0);

	public static AmazonSQSClient sqs = null;
	public static String awsAccessKey = "AKIAJFSBEF3PYJ7SNOCQ";
	public static String awsSecretKey = "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";
	public static Object dashboardFileName = "Inactive 10 Day All Users";
	public static MemcachedClient memcacheObj;

	public static String totalUsersCount = "";
	public static String totalUserKey = "inactive10dayuser_totalUserKey";
	public static String whiteLabelNameKey = "inactive10dayuser_whiteLabelNameKey";
	// public static String processStartTime =
	// "inactive10dayuser_processStartTime";
	public static String userProcessed = "inactive10dayuser_userProcessed";
	public static String emailSend = "inactive10dayuser_emailSend";
	public static String duplicateUserKey = "inactive10dayuser_duplicateUserKey";
	public static String perSecondsMails = "inactive10dayuser_perSecondsMails";
	public static String processCurrentExecutionTime = "inactive10dayuser_processCurrentExecutionTime";
	public static String perSecondUsers = "inactive10dayuser_perSecondUsers";
	public static Map<String, Integer> child_Process_stats_hashMap = Collections.synchronizedMap(new HashMap<String, Integer>());
	public static String processesIsRunning = "0";
	public static boolean isProcesskilled = false;
	public static int bccSendEmailIntervalCount = 100001;
	public static int bccSendEmailIntervalPercentage = 30;

	public static String server189InstanceId = "i-9ceb9534";
	public static String server62InstanceId = "i-5d9ceef5";
	public static String server7InstanceId = "i-d436b07c";
	public static String server235InstanceId = "i-3416539f";
	public static String server109InstanceId = "i-9db6a735";
	public static String server98InstanceId = "i-0c44166521af2ad95";
	public static String killserverFilePath = "/var/nfs-93/redirect/mis_logs/Java_process_stats/";
	public static String ec2AwsAccessKey = "AKIAIJJZZVULVKZNE7RQ";
	public static String ec2AwsSecretKey = "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";
	// New server 98 access and secret key
	public static String ec2_98AwsAccessKey = "AKIAI6ISH4ZK4DMLB7UA";
	public static String ec2_98AwsSecretKey = "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";
	public static String subjectLineFileName = "InActiveTemplateConfig";

	public static ArrayList<GroupObject> subjectLIneList = new ArrayList<GroupObject>();

}
