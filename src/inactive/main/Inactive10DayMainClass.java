package inactive.main;

import inactive.emailsend.SendEmail;
import inactive.models.GroupObject;
import inactive.models.UsersData;
import inactive.models.WhiteLabelParameterModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import net.spy.memcached.MemcachedClient;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Inactive10DayMainClass {

	public static Inactive10DayMainClass mInactive10DayMainClassObject = null;
	public static SendEmail mSendEmailObject = null;
	public static ArrayList<String> categoriesList = new ArrayList<String>();
	public static ArrayList<String> childDomainMemCacheArrayList = new ArrayList<String>();
	public static long processStartTime;
	public static boolean readLiveStatsFile = true;
	public static String startTimeForDashBoard = "";

	public static void main(String args[]) {
		try {
			SettingsClass.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
		} catch (Exception e) {
			e.printStackTrace();
		}
		startTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
		processStartTime = System.currentTimeMillis();

		String emailForTesting = "";
		try {

			if (args != null && args[0] != null) {
				SettingsClass.filePath = "/var/nfs-93/redirect/mis_logs/";
				String processName = args[0];

				String processType = "";
				try {
					processType = args[1].trim();
				} catch (Exception e) {
				}

				if (!processType.equalsIgnoreCase("")) {

					if (processType.equalsIgnoreCase("runtest")) {
						SettingsClass.isTestRun = true;

					} else {
						System.out.println("Run time Parameters not match! Please try again");
						System.exit(0);
					}

				} else {
					SettingsClass.isTestRun = false;
					SettingsClass.TOTAL_N0_THREAD = 3;
				}
				try {
					emailForTesting = args[2].trim();
				} catch (Exception e) {
					emailForTesting = "";
				}
				if (!emailForTesting.equalsIgnoreCase(""))
					SettingsClass.testingEmailId = emailForTesting;

			} else {
				System.out.println("Run time Parameters not found! Please try again");
				System.exit(0);
			}
		} catch (Exception e) {
		}
		SettingsClass.subjectLIneList = readAllSubjectLines(SettingsClass.subjectLineFileName);
		readFiles();
		readStatsFromMemCache();

		mInactive10DayMainClassObject = new Inactive10DayMainClass();
		mSendEmailObject = new SendEmail();

		try {
			new Thread(new writeDataThread()).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		mInactive10DayMainClassObject.getDataFromQueue();
	}

	private static void readStatsFromMemCache() {
		try {
			String userProcessed = (String) SettingsClass.memcacheObj.get(SettingsClass.userProcessed);
			if (userProcessed != null)
				SettingsClass.userProcessingCount.set(Integer.parseInt(userProcessed));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {
		}
		try {
			String lastEmailSent = (String) SettingsClass.memcacheObj.get(SettingsClass.emailSend);
			if (lastEmailSent != null)
				SettingsClass.totalEmailCount.set(Integer.parseInt(lastEmailSent));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {
		}

		if (readLiveStatsFile)
			readStatsFromFile();

		readChildDomainsStatsFromFile();

	}

	private static void readStatsFromFile() {
		BufferedReader bufferedReader = null;
		String readLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "dashboard" + File.separator + SettingsClass.queueName + "_live_stats.txt"));
			readLine = bufferedReader.readLine();
			String Data[] = readLine.toString().split("\\|");
			int runningState = Integer.parseInt(Data[10]);
			boolean TodayDate = true;
			Date date1 = null, date2 = null;
			Calendar calendar = new GregorianCalendar();
			String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			try {

				date1 = sdf.parse(currentdate);

				date2 = sdf.parse(Data[0]);

			} catch (ParseException e) {

				e.printStackTrace();
			}

			if (date1.equals(date2)) {
				TodayDate = true;
			} else {
				TodayDate = false;
			}

			if (runningState == 0 && TodayDate) {
				// process has restarted
				SettingsClass.userProcessingCount.set(Integer.parseInt(Data[2]));
				SettingsClass.totalEmailCount.set(Integer.parseInt(Data[4]));
			}

		} catch (Exception e) {

		}

	}

	private static void readChildDomainsStatsFromFile() {
		BufferedReader bufferedReader = null;
		String readLine = null;
		try {
			for (int i = 0; i < Inactive10DayMainClass.childDomainMemCacheArrayList.size(); i++) {

				String childKey = Inactive10DayMainClass.childDomainMemCacheArrayList.get(i);

				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "combine_process_dashboard/" + childKey + "_live_stats.txt"));
				readLine = bufferedReader.readLine();
				String Data[] = readLine.toString().split("\\|");
				int runningState = Integer.parseInt(Data[10]);
				boolean TodayDate = true;
				Date date1 = null, date2 = null;
				Calendar calendar = new GregorianCalendar();
				String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));
				try {
					date1 = SettingsClass.dateFormat.parse(currentdate);
					date2 = SettingsClass.dateFormat.parse(Data[0]);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (date1.equals(date2)) {
					TodayDate = true;
				} else {
					TodayDate = false;
				}
				synchronized (SettingsClass.child_Process_stats_hashMap) {

					if (runningState == 0 && TodayDate) {
						// here we set the stats in the memecache from the file
						SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.userProcessed, 0, Integer.parseInt(Data[2]));
						SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.emailSend, 0, Integer.parseInt(Data[4]));
						SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.userProcessed, Integer.parseInt(Data[2]));
						SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.emailSend, Integer.parseInt(Data[4]));
					}
				}
			}

		} catch (Exception e) {

		}
	}

	public AmazonSQSClient intilizeSqs() {
		AmazonSQSClient sqs;
		AWSCredentials awsCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);
		sqs = new AmazonSQSClient(awsCredentials);
		sqs.setRegion(Region.getRegion(Regions.US_EAST_1));
		return sqs;

	}

	public void getDataFromQueue() {
		SettingsClass.sqs = intilizeSqs();
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SettingsClass.queueName);
		if (SettingsClass.isTestRun)
			receiveMessageRequest.setMaxNumberOfMessages(1);
		else
			receiveMessageRequest.setMaxNumberOfMessages(10);
		receiveMessageRequest.getMessageAttributeNames();
		receiveMessageRequest.getAttributeNames();

		String userQueueUrl = SettingsClass.baseQueurl + SettingsClass.queueName;

		int queueSize = JobSearchThread.getQueueSize(userQueueUrl);

		System.out.println("\n User Queue Url..." + userQueueUrl);
		System.out.println("\n QUEUE data size..." + queueSize);
		System.out.println("\n Thread count for this QUEUE ..." + SettingsClass.TOTAL_N0_THREAD);

		if (queueSize == 0 && !SettingsClass.isTestRun) {
			String text = SettingsClass.dashboardFileName + " has 0 data in the queue";
			Utility.sendTextSms(text);
			System.exit(0);
		}

		for (int i = 0; i < SettingsClass.TOTAL_N0_THREAD; i++) {

			JobSearchThread jobSearchThread = new JobSearchThread(receiveMessageRequest, userQueueUrl);
			jobSearchThread.start();

		}

	}

	private static ArrayList<GroupObject> readAllSubjectLines(String fileName) {

		BufferedReader bufferedReader = null;
		ArrayList<GroupObject> localGroupList = new ArrayList<GroupObject>();
		try {
			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt"));
			localGroupList = new Gson().fromJson(bufferedReader, new TypeToken<List<GroupObject>>() {
			}.getType());

			for (int i = 0; i < localGroupList.size(); i++) {
				if (localGroupList.get(i).getActive().equalsIgnoreCase("0")) {
					localGroupList.remove(localGroupList.get(i));
					i = i - 1;
				}

			}
		} catch (Exception e) {
			Utility.sendTextSms("Subject From Name File not found " + SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt Please check...");
		}

		System.out.println("size of list=" + localGroupList.size());
		return localGroupList;

	}

	public static void readFiles() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "Java_process_stats/" + SettingsClass.queueName + "_size.txt"));

			// read version line(first line)
			String line1 = null;

			line1 = bufferedReader.readLine();

			while (line1 != null) {

				String[] totalAndDomainChildMemchahe = line1.split("\\|");
				if (totalAndDomainChildMemchahe[2].toLowerCase().contains("global")) {
					String Globalkey = totalAndDomainChildMemchahe[2].replace(" ", "").trim();
					SettingsClass.totalUsersCount = totalAndDomainChildMemchahe[0].replace(" ", "").trim();
					SettingsClass.totalUserKey = Globalkey + SettingsClass.totalUserKey;
					// SettingsClass.processStartTime = Globalkey +
					// SettingsClass.processStartTime;
					SettingsClass.userProcessed = Globalkey + SettingsClass.userProcessed;
					SettingsClass.emailSend = Globalkey + SettingsClass.emailSend;
					SettingsClass.duplicateUserKey = Globalkey + SettingsClass.duplicateUserKey;
					SettingsClass.perSecondsMails = Globalkey + SettingsClass.perSecondsMails;
					SettingsClass.processCurrentExecutionTime = Globalkey + SettingsClass.processCurrentExecutionTime;
					SettingsClass.perSecondUsers = Globalkey + SettingsClass.perSecondUsers;
					try {
						SettingsClass.memcacheObj.set(SettingsClass.totalUserKey, 0, SettingsClass.totalUsersCount);
					} catch (Exception e) {

						e.printStackTrace();
					}
					// for group and templte category wise keys
					Inactive10DayMainClass.categoriesList.add(totalAndDomainChildMemchahe[4].replace(" ", "_").trim());

				} else {

					// we need to set up child memcahe keys ...Total

					String childKeys = totalAndDomainChildMemchahe[1].replace(" ", "").trim() + "_" + totalAndDomainChildMemchahe[2].replace(" ", "").trim();
					String whitelabelName = totalAndDomainChildMemchahe[7];
					// this is the child mem key..
					Inactive10DayMainClass.childDomainMemCacheArrayList.add(childKeys);

					// System.out.println("\n\n\n\n\n\n childKeys :" +
					// childKeys);

					try {
						// here we set the total count values of all child
						// domain processes..
						// setttng the totoal users count for the child
						// process in the map
						synchronized (SettingsClass.child_Process_stats_hashMap) {
							SettingsClass.memcacheObj.set(childKeys + "_" + SettingsClass.totalUserKey, 0, totalAndDomainChildMemchahe[0].trim());

							SettingsClass.memcacheObj.set(childKeys + "_" + SettingsClass.whiteLabelNameKey, 0, whitelabelName.trim());

							SettingsClass.child_Process_stats_hashMap.put(childKeys + "_" + SettingsClass.totalUserKey, Integer.parseInt(totalAndDomainChildMemchahe[0].trim()));

						}
					} catch (Exception e) {

						e.printStackTrace();
					}

					// for group and templte category wise keys
					Inactive10DayMainClass.categoriesList.add(totalAndDomainChildMemchahe[2].replace(" ", "_").trim());

				}

				line1 = bufferedReader.readLine();
			}
		} catch (Exception e2) {

			try {
				// not required but use if file not found in read api
				// settings thread

				String totalusers = (String) SettingsClass.memcacheObj.get(SettingsClass.totalUserKey);

				if (totalusers != null && SettingsClass.totalUsersCount.equalsIgnoreCase(""))
					SettingsClass.totalUsersCount = totalusers;

			} catch (Exception e) {

				e.printStackTrace();
			}

			e2.printStackTrace();
		}

		// here we will calculate the bcc percenatage...
		if (!SettingsClass.isTestRun) {

			try {
				SettingsClass.bccSendEmailIntervalCount = (Integer.parseInt(SettingsClass.totalUsersCount) * SettingsClass.bccSendEmailIntervalPercentage) / 100;
			} catch (Exception e) {

			}

		}

	}

}

class writeDataThread implements Runnable {

	@Override
	public void run() {

		while (true) {

			try {
				Thread.sleep(3 * 60 * 1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Utility.updateMemcacheStatsInEnd();

		}

	}

}

class JobSearchThread extends Thread {

	List<UsersData> userDataList;
	ReceiveMessageRequest receiveMessageRequest;
	String userQueueName = "", userQueueUrl = "";
	AtomicInteger localGroupThreadCount = new AtomicInteger(0);

	JobSearchThread(ReceiveMessageRequest receiveMessageRequest, String userQueueUrl) {

		this.receiveMessageRequest = receiveMessageRequest;
		this.userQueueUrl = userQueueUrl;

	}

	@Override
	public void run() {

		while (true) {
			try {

				SettingsClass.cal = Calendar.getInstance();

				if (SettingsClass.cal.get(Calendar.HOUR_OF_DAY) >= 23 && SettingsClass.cal.get(Calendar.MINUTE) >= 30) {
					SettingsClass.processesIsRunning = "1";
					Utility.updateMemcacheStatsInEnd();
					Utility.killProcess();

				}

				// if emptyUserMessageCount is more then 100 then we will not go
				// for fetching users from queue...
				if (SettingsClass.emptyUserMessageCount.get() <= 100) {

					List<Message> messages = SettingsClass.sqs.receiveMessage(receiveMessageRequest).getMessages();

					if (messages.size() == 0) {

						// reset the empty queue count..
						SettingsClass.emptyUserMessageCount.incrementAndGet();

					} else {

						// reset the empty queue count..
						SettingsClass.emptyUserMessageCount.set(0);

						for (Message message : messages) {

							// split the multiple users into array
							String[] usersList = message.getBody().split("\n");

							for (String user : usersList) {
								String[] bodyData = user.split("\\|");
								UsersData userDataObject = new UsersData();

								try {
									userDataObject.id = bodyData[0];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.keyword = (URLDecoder.decode(Utility.html2text(bodyData[1].trim()), "UTF-8"));
									if (userDataObject.keyword.contains("$pipe$")) {
										userDataObject.keyword.replace("$pipe$", "|");
									}
								} catch (Exception e2) {
									userDataObject.keyword = (bodyData[1].trim());
									if (userDataObject.keyword.contains("$pipe$")) {
										userDataObject.keyword.replace("$pipe$", "|");
									}
									e2.printStackTrace();
								}

								try {
									userDataObject.zipcode = bodyData[2];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.email = bodyData[3];
									userDataObject.email = userDataObject.email.replace(" ", "");
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.city = bodyData[6];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.state = bodyData[7];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.firstName = bodyData[17];

									if (userDataObject.firstName == null || userDataObject.firstName.equalsIgnoreCase("") || userDataObject.firstName.equalsIgnoreCase("null")) {
										userDataObject.firstName = "";
									}

								} catch (Exception e2) {

									// e2.printStackTrace();
									userDataObject.firstName = "";
								}

								try {
									userDataObject.comapaingId = bodyData[32];
								} catch (Exception e2) {
								}

								try {
									userDataObject.domainName = bodyData[33];
								} catch (Exception e2) {
								}
								try {
									userDataObject.userSourceNameForHtml = bodyData[27];
								} catch (Exception e2) {
								}

								try {
									userDataObject.sendGridCategory = bodyData[36];
								} catch (Exception e2) {
								}

								try {
									userDataObject.campgain_category_mapping_key = bodyData[38];
								} catch (Exception e2) {
								}

								try {
									userDataObject.whitelabel_name = bodyData[40];
								} catch (Exception e2) {
									e2.printStackTrace();
								}

								try {
									WhiteLabelParameterModel mWhiteLabelParameterModel = SettingsClass.doimanWiseHashMap.get(userDataObject.domainName + "_" + userDataObject.whitelabel_name);
									userDataObject.fromDomainName = mWhiteLabelParameterModel.FromEmailAddress;
									userDataObject.sendGridUserName = mWhiteLabelParameterModel.SendgridUsername;
									userDataObject.sendGridUserPassword = mWhiteLabelParameterModel.SendgridPassword;
									userDataObject.themeColor = mWhiteLabelParameterModel.themeColor;
									userDataObject.logoUrl = mWhiteLabelParameterModel.logoUrl;
									userDataObject.HostDomainName = mWhiteLabelParameterModel.HostDomainName;
									userDataObject.postalAddress = mWhiteLabelParameterModel.postalAddress;
									userDataObject.sparkpost_key = mWhiteLabelParameterModel.sparkpost_key;

								} catch (Exception e2) {
								}

								Object duplicateUserEmailObject = null;
								try {
									duplicateUserEmailObject = SettingsClass.memcacheObj.get(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_" + userDataObject.zipcode.replace(" ", "") + "_" + SettingsClass.queueName + "_" + userDataObject.whitelabel_name);

								} catch (Exception e) {

									e.printStackTrace();
								}

								if (duplicateUserEmailObject == null) {
									SettingsClass.userProcessingCount.incrementAndGet();
									SettingsClass.newuserProcessingCount.incrementAndGet();

									try {

										Integer childUserProcessed = null;
										String tempKey = "";

										if (userDataObject.sendGridCategory.contains("default value")) {
											tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
										} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
											tempKey = "_" + userDataObject.comapaingId.replace(" ", "").trim();
										} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
											tempKey = "_" + userDataObject.comapaingId.replace(" ", "").trim();
										} else {
											tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
										}

										String childUserProcessedKeyStr = userDataObject.domainName.replace(" ", "").trim() + tempKey + "_" + SettingsClass.userProcessed;

										synchronized (SettingsClass.child_Process_stats_hashMap) {

											try {
												childUserProcessed = SettingsClass.child_Process_stats_hashMap.get(childUserProcessedKeyStr);
											} catch (Exception e) {
												childUserProcessed = 0;
											}
											if (childUserProcessed == null)
												childUserProcessed = 0;
											childUserProcessed++;
											SettingsClass.memcacheObj.set(childUserProcessedKeyStr, 0, String.valueOf(childUserProcessed));
											SettingsClass.child_Process_stats_hashMap.put(childUserProcessedKeyStr, childUserProcessed);
										}
									} catch (Exception e) {
									}

									try {

										SettingsClass.memcacheObj.set(SettingsClass.userProcessed, 0, String.valueOf(SettingsClass.userProcessingCount));

									} catch (Exception e) {

										e.printStackTrace();
									}

									try {
										SettingsClass.memcacheObj.set(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_" + userDataObject.zipcode.replace(" ", "") + "_" + SettingsClass.queueName + "_" + userDataObject.whitelabel_name, 0, "0");

									} catch (Exception e1) {

										e1.printStackTrace();
									}

									try {

										if (localGroupThreadCount.get() >= SettingsClass.subjectLIneList.size())
											localGroupThreadCount.set(0);

										if (!userDataObject.email.equalsIgnoreCase("") && !userDataObject.zipcode.equalsIgnoreCase("")) {

											if (userDataObject.sendGridCategory.contains("default value")) {
												Inactive10DayMainClass.mSendEmailObject.sendEmailVieSes(userDataObject);
											} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
												Inactive10DayMainClass.mSendEmailObject.sendEmailViaSparkPost(userDataObject);
											} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
												userDataObject.unsubTemplink = "<a title = 'unsubscribe' href = \"%unsubscribe_url%\"></a>";
												Inactive10DayMainClass.mSendEmailObject.sendEmailViaMailgun(userDataObject, localGroupThreadCount.get());
											} else {
												userDataObject.unsubTemplink = "<a title = 'unsubscribe' href = \"<UNSUBSCRIBE>\"></a>";
												Inactive10DayMainClass.mSendEmailObject.sendEmailViaSendGrid(userDataObject);
											}

										}
										localGroupThreadCount.getAndIncrement();

									} catch (Exception e) {

									}
									// }

								} else {

									// 0 means email was not sent to user, try
									// to re process that user
									if (duplicateUserEmailObject.equals("0")) {
										if (localGroupThreadCount.get() >= SettingsClass.subjectLIneList.size())
											localGroupThreadCount.set(0);

										try {

											if (!userDataObject.email.equalsIgnoreCase("") && !userDataObject.zipcode.equalsIgnoreCase("")) {

												if (userDataObject.sendGridCategory.contains("default value")) {
													Inactive10DayMainClass.mSendEmailObject.sendEmailVieSes(userDataObject);
												} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
													Inactive10DayMainClass.mSendEmailObject.sendEmailViaSparkPost(userDataObject);
												} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
													userDataObject.unsubTemplink = "<a title = 'unsubscribe' href = \"%unsubscribe_url%\"></a>";
													Inactive10DayMainClass.mSendEmailObject.sendEmailViaMailgun(userDataObject, localGroupThreadCount.get());
												} else {
													userDataObject.unsubTemplink = "<a title = 'unsubscribe' href = \"<UNSUBSCRIBE>\"></a>";
													Inactive10DayMainClass.mSendEmailObject.sendEmailViaSendGrid(userDataObject);
												}
											}

											localGroupThreadCount.getAndIncrement();

										} catch (Exception e) {

										}
										// }
									} else {

										// tracking the count of duplicate
										// users.....

										SettingsClass.duplicateUser.getAndIncrement();

										try {
											SettingsClass.memcacheObj.set(SettingsClass.duplicateUserKey, 0, String.valueOf(SettingsClass.duplicateUser.get()));
										} catch (Exception e) {

											e.printStackTrace();
										}
									}
								}

							}

							// deleting the msg after full processing..now we
							// are processing max 125 users in a msg..
							try {
								String messageRecieptHandle = message.getReceiptHandle();
								SettingsClass.sqs.deleteMessage(new DeleteMessageRequest(this.userQueueUrl, messageRecieptHandle));
							} catch (Exception e) {

								e.printStackTrace();
							}

							// here exit from the running process on specific
							// mail count...

							if (SettingsClass.isTestRun) {
								Utility.updateMemcacheStatsInEnd();
								System.exit(0);
							}
							// }

						}

					}

				} // end of else part..

				// here we will call for getting queue size and flight msg queue
				// size..

				if (SettingsClass.emptyUserMessageCount.get() > 100) {

					if (SettingsClass.emptyQueueCount.get() < 10) {

						// getting queue size from the aws server
						int queueSize = getQueueSize(userQueueUrl);
						// getting size of flight messages
						int flightQueueSize = getQueueSizeInFlight(userQueueUrl);

						if (queueSize == 0 && flightQueueSize == 0) {

							SettingsClass.emptyQueueCount.incrementAndGet();
						} else if (queueSize != 0) {

							SettingsClass.emptyQueueCount.set(0);
							SettingsClass.emptyUserMessageCount.set(0);

						} else if (flightQueueSize != 0) {

							SettingsClass.emptyQueueCount.set(0);

							sleep(1000 * 60 * 1);
							System.out.println(" \n Process slept!");

						}

					} else {
						SettingsClass.processesIsRunning = "1";
						Utility.updateMemcacheStatsInEnd();
						Utility.killProcess();

					}

				}

			} catch (Exception e) {
			}

		}
	}

	public static int getQueueSize(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessages");
		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessages"));
		} catch (Exception e) {

		}

		return size;

	}

	// reading quesize in
	// floght=======================================================

	public int getQueueSizeInFlight(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessagesNotVisible");

		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessagesNotVisible"));
		} catch (Exception e) {

		}

		return size;

	}

}
