package inactive.main;

import inactive.models.WhiteLabelParameterModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.util.EC2MetadataUtils;

public class Utility {
	public static void sendTextSms(String data) {
		try {
			String phoneNumber = "8860547656,8699864636,9530667082,9855115882,9988736500,9464533885";
			data = URLEncoder.encode(data, "UTF-8");
			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";
			url = url + phoneNumber;
			String text = "&text=" + data;
			url = url + text;
			url = url + "&priority=ndnd&stype=normal";
			System.out.println("Url=" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String decodeString(String data) {

		try {
			data = URLDecoder.decode((html2text(data.replace("$", "").replace("!", ""))), "UTF-8");
		} catch (Exception e) {
			try {
				data = html2text(data.replace("$", "").replace("!", ""));
			} catch (Exception e2) {
				data = data.replace("$", "").replace("!", "");
			}

		}

		return data;

	}

	public static String html2text(String html) {

		try {
			return Jsoup.parse(html).text();
		} catch (Exception e) {

		}

		return "";
	}

	public static void createDomainWiseHashMap() {
		Connection con = openConnection();
		ResultSet rs = null;

		String query = "select DomainUrl,MailgunDomainName,FromEmailAddress,SendgridUsername,SendgridPassword,"
				+ "EmailClient,PostalAddress,LearnMoreColorCode,HostDomainName,LogoUrl,whitelabel_name,sparkpost_key from whitelabels_process";

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			rs = pst.executeQuery();
			WhiteLabelParameterModel mWhiteLabelParameterModel = null;
			while (rs.next()) {
				String key = rs.getString("DomainUrl");

				if (key == null)
					continue;

				String emailClient = rs.getString("EmailClient");
				if (emailClient.equalsIgnoreCase("mailgun")) {
					key = rs.getString("MailgunDomainName");
				}

				key += "_" + rs.getString("whitelabel_name");

				mWhiteLabelParameterModel = new WhiteLabelParameterModel();
				mWhiteLabelParameterModel.FromEmailAddress = rs.getString("FromEmailAddress");
				mWhiteLabelParameterModel.SendgridUsername = rs.getString("SendgridUsername");
				mWhiteLabelParameterModel.SendgridPassword = rs.getString("SendgridPassword");
				mWhiteLabelParameterModel.postalAddress = rs.getString("PostalAddress");
				mWhiteLabelParameterModel.themeColor = rs.getString("LearnMoreColorCode");
				mWhiteLabelParameterModel.HostDomainName = rs.getString("HostDomainName");
				mWhiteLabelParameterModel.logoUrl = rs.getString("LogoUrl");
				mWhiteLabelParameterModel.whitelabelName = rs.getString("whitelabel_name");
				mWhiteLabelParameterModel.sparkpost_key = rs.getString("sparkpost_key");

				SettingsClass.doimanWiseHashMap.put(key, mWhiteLabelParameterModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}

	}

	public static synchronized void killProcess() {

		if (!SettingsClass.isProcesskilled) {
			SettingsClass.isProcesskilled = true;
			// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(SettingsClass.dashboradFileName
			// + " has been completed successfully", "stop");
			Utility.stopServer();
			System.exit(0);

		}

	}

	public static void stopServer() {

		AmazonEC2 ec2;
		AWSCredentials ec2AwsCredentials;
		boolean stopServer = false;
		int processCount = 2;

		try {
			String instanceId = EC2MetadataUtils.getInstanceId();
			System.out.println(" \ninstanceId..." + instanceId);

			if (instanceId.contains(SettingsClass.server98InstanceId)) {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2_98AwsAccessKey, SettingsClass.ec2_98AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
				instanceId = EC2MetadataUtils.getInstanceId();
			} else {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2AwsAccessKey, SettingsClass.ec2AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
			}

			String fileName = "";
			if (instanceId.contains(SettingsClass.server189InstanceId)) {
				fileName = "server189_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server62InstanceId)) {
				fileName = "server62_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server7InstanceId)) {
				fileName = "server7_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server235InstanceId)) {
				fileName = "server235_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server109InstanceId)) {
				fileName = "server109_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server98InstanceId)) {
				fileName = "server98_on_off_stats.txt";
			}

			BufferedReader bufferedReader = null;

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.killserverFilePath + fileName));

				// bufferedReader = new BufferedReader(new
				// FileReader("/home/sandeep/Desktop/Drive_D/AWS
				// jars/july/"+fileName));

				processCount = Integer.parseInt(bufferedReader.readLine());

				System.out.println(processCount + "");

				if (processCount == 0) {
					stopServer = true;
				} else {
					// decrease process count by 1
					processCount = processCount - 1;

					if (processCount == 0) {
						stopServer = true;
					}
				}

				bufferedReader = null;

			} catch (Exception e2) {
				//
				e2.printStackTrace();
			}

			if ((instanceId.contains(SettingsClass.server189InstanceId) || instanceId.contains(SettingsClass.server62InstanceId) || instanceId.contains(SettingsClass.server7InstanceId)
					|| instanceId.contains(SettingsClass.server235InstanceId) || instanceId.contains(SettingsClass.server109InstanceId) || instanceId.contains(SettingsClass.server98InstanceId))
					&& stopServer) {

				System.out.println(" \n Entered in block of stop server");

				// Stop Ec2 Instance
				StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instanceId);
				StopInstancesResult stopResult = ec2.stopInstances(stopRequest);

				System.out.println(" \nstopResult..." + stopResult);
			}
			// update process count in file

			BufferedWriter bufferedWriter = null;
			try {
				bufferedWriter = new BufferedWriter(new FileWriter(SettingsClass.killserverFilePath + fileName));

				bufferedWriter.write(String.valueOf(processCount));

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					bufferedWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (AmazonServiceException e) {
			//
			e.printStackTrace();
		} catch (AmazonClientException e) {
			//
			e.printStackTrace();
		} catch (Exception e) {
			//
			e.printStackTrace();
		}

	}

	public static void updateMemcacheStatsInEnd() {

		// Update per second email count
		try {
			Long TimeDifference = System.currentTimeMillis() - Inactive10DayMainClass.processStartTime;
			String TimeDifferenceEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / (TimeDifference / 1000));
			SettingsClass.memcacheObj.set(SettingsClass.perSecondsMails, 0, TimeDifferenceEmailCount);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// total users processed in persecond
		Long TimeDifference = System.currentTimeMillis() - Inactive10DayMainClass.processStartTime;
		float usersProcssed = SettingsClass.userProcessingCount.get() / (TimeDifference / 1000);
		try {
			System.out.println(" \ntotal users processed in persecond..." + usersProcssed);
			SettingsClass.memcacheObj.set(SettingsClass.perSecondUsers, 0, String.valueOf(usersProcssed));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		updateFilesData();

	}

	public static void updateFilesData() {

		System.out.println("Writing data in files");

		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		FileWriter fileWriterObj = null;
		File log_dashboard_folder = null;
		File child_log_dashboard_folder = null;

		try {

			DecimalFormat decFormat = new DecimalFormat("###.##");
			try {

				try {
					log_dashboard_folder = new File(SettingsClass.filePath + "dashboard/");
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (!log_dashboard_folder.exists()) {
					log_dashboard_folder.mkdir();
				}
				try {
					child_log_dashboard_folder = new File(SettingsClass.filePath + "combine_process_dashboard/");
				} catch (Exception e) {

					e.printStackTrace();
				}
				if (!child_log_dashboard_folder.exists()) {
					child_log_dashboard_folder.mkdir();
				}

			} catch (Exception e1) {
				//
				e1.printStackTrace();
			}

			// =================== Live stats update
			try {
				fileWriterObj = new FileWriter(log_dashboard_folder + File.separator + SettingsClass.queueName + "_live_stats.txt");
			} catch (Exception e) {

			}

			// Today DateTime|TOTAL USERS|USER PROCESSED|PER SECOND
			// PROCESSED USERS|MAIL SENT|PER SECOND EMAILS|PROCESS START
			// TIME|CLOUD
			// SEARCH USERS PERCENTAGE|MAIL CLOUD JOBS PERCENTAGE|PROCESS NAME

			// 2015-08-04|223422|21345|70|20012|55|6:55:3|32.09|20.34|oakjob
			// openers
			// MAIL CLOUD JOBS PERCENTAGE

			Long TimeDifference = System.currentTimeMillis() - Inactive10DayMainClass.processStartTime;
			Long timeInSeconds = (TimeDifference / 1000);
			double perSecondUsersProcssed = SettingsClass.newuserProcessingCount.get() / timeInSeconds;
			String perSecondPercentageEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / timeInSeconds);

			String stopTime = "";
			if (SettingsClass.processesIsRunning.equalsIgnoreCase("1"))
				stopTime = SettingsClass.processTimeDateFormat.format(new Date());

			try {
				String writeString = df1.format(new Date()) + "|" + SettingsClass.totalUsersCount + "|" + SettingsClass.userProcessingCount.get() + "|" + decFormat.format(perSecondUsersProcssed)
						+ "|" + SettingsClass.totalEmailCount + "|" + perSecondPercentageEmailCount + "|" + Inactive10DayMainClass.startTimeForDashBoard + "|0" + "|0" + "|"
						+ SettingsClass.dashboardFileName + "|" + SettingsClass.processesIsRunning + "|0" + "|0" + "|" + stopTime + "|" + SettingsClass.duplicateUser.get() + "|0";

				fileWriterObj.write(writeString);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fileWriterObj.close();
			}

			// child process
			synchronized (SettingsClass.child_Process_stats_hashMap) {

				for (int i = 0; i < Inactive10DayMainClass.childDomainMemCacheArrayList.size(); i++) {

					String childDomainName = Inactive10DayMainClass.childDomainMemCacheArrayList.get(i);

					try {
						fileWriterObj = new FileWriter(child_log_dashboard_folder + File.separator + childDomainName + "_live_stats.txt");

					} catch (Exception e) {

					}

					int totalUsersCount = 0;
					try {
						totalUsersCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.totalUserKey);
					} catch (Exception e1) {
					}
					int userProcessingCount = 0;
					try {
						userProcessingCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.userProcessed);
					} catch (Exception e1) {
					}
					int totalEmailCount = 0;
					try {
						totalEmailCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.emailSend);
					} catch (Exception e1) {

					}
					String whiteLabelName = "";
					try {
						whiteLabelName = (String) SettingsClass.memcacheObj.get(childDomainName + "_" + SettingsClass.whiteLabelNameKey);
					} catch (Exception e1) {
						e1.printStackTrace();
					}

					int elasticDuplicateCount = 0;
					try {
						String writeString = df1.format(new Date()) + "|" + totalUsersCount + "|" + userProcessingCount + "|" + 0 + "|" + totalEmailCount + "|" + 0 + "|"
								+ Inactive10DayMainClass.startTimeForDashBoard + "|" + 0 + "|" + 0 + "|" + childDomainName + "|" + SettingsClass.processesIsRunning + "|" + 0 + "|" + 0 + "|"
								+ stopTime + "|" + SettingsClass.duplicateUser.get() + "|0" + "|" + SettingsClass.dashboardFileName + "|" + whiteLabelName + "|" + elasticDuplicateCount;

						fileWriterObj.write(writeString);

						// System.out.println("writing done..........");

					} catch (Exception e) {

						e.printStackTrace();
					}

					try {
						fileWriterObj.close();
					} catch (Exception e) {

						e.printStackTrace();
					}

				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static Connection openConnection() {
		String HOST_NAME = "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";
		String DB_NAME = "oakalerts";
		String username = "awsoakuser";
		String password = "awsoakusersignity";
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username, password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}
}
